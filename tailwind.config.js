const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors')

module.exports = {
  content: [
      './storage/framework/views/*.php',
      './resources/**/*.blade.php',
      './resources/**/*.js',
      './resources/**/*.vue',
      './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
      './vendor/filament/**/*.blade.php',
  ],
    darkMode: 'class',
    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                transparent: 'transparent',
                current: 'currentColor',
                black: colors.black,
                white: colors.white,
                green: colors.green,
                indigo: colors.indigo,
                red: colors.red,
                alojatuweb: '#85b542',
                alojatuwebdark: '#103c10',
                danger: colors.rose,
                primary: colors.emerald,
                success: colors.green,
                warning: colors.yellow,
            }
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },
  plugins: [
      require('@tailwindcss/forms'),
      require('@tailwindcss/typography'),
  ],
}
