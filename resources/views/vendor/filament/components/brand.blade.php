@if (file_exists('img/logo.png'))
    <img src="{{ asset('/img/logo.png') }}" alt="Logo" class="h-10">
@else
    @if (filled($brand = config('filament.brand')))
        <div @class([
            'text-xl font-bold tracking-tight filament-brand',
            'dark:text-white' => config('filament.dark_mode'),
        ])>
            {{ $brand }}
        </div>
    @endif
@endif
